//package name
package belajarHitapidua;

//import liblary parse json
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static belajarHitapi.belajarHitapi.hitApi;
import static belajarHitapi.belajarHitapi.md5Java;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import sun.applet.Main;

//import koneksi postgres
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Calendar;

public class hitApijson {
    public static Connection con;
    public static Statement stm;
    
    public static String md5Java(String message) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String convert = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            //merubah byte array ke dalam String Hexadecimal
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            convert = sb.toString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return convert;
    }

    public static void proseshitdataApi() {
        
    	String username = "plg12110004";
        String password = "aldwin.3";
        String secret_key = "1234";
        String url = "http://180.211.90.243:8118/Transactions/trx.json";
        String trxType = "2100";
        String trxId = "0";
        String custMsisdn = "212100089886";
        String custAccountNo = "212100089886";
        String prodId = "100";
        Random random = new Random();
        trxId = Integer.toString(random.nextInt(99999999));
              
        try {
            URL obj = new URL(url);
            
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setConnectTimeout(100000);
            con.setReadTimeout(100000); 
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");	
            String trxDate = sdf.format(new Date());
    		
            String signature = md5Java(username + password + prodId + trxDate + secret_key);
            
            String urlParameters =
                "trx_date="+trxDate+
                "&trx_type="+trxType+
                "&trx_id="+trxId+
                "&cust_msisdn="+custMsisdn+
                "&cust_account_no="+custAccountNo+
                "&product_id="+prodId+
                "&product_nomination="+
                "&periode_payment=";

            // add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Authorization", "PELANGIREST username="+username+"&password="+password+"&signature="+signature);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();	

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            // print result
//            System.out.println(response.toString());

            //send to parse data           
            JSONObject data = new JSONObject();
            data.put("trx_date", trxDate);
            data.put("trx_type", trxType);
            data.put("trx_id", trxId);
            data.put("cust_msisdn", custMsisdn);
            data.put("cust_account_no", custAccountNo);
            data.put("product_id", prodId);
            data.put("product_nomination","");
            data.put("periode_payment","");     
            
            String datarequest = data.toString();
            
            parsedataApi(response,datarequest,prodId);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static String parsedataApi(StringBuffer dataapi, String datarequst, String prodId) throws NoSuchAlgorithmException, UnsupportedEncodingException, SQLException {       
        
        JSONParser jp = new JSONParser();
        String dtjson = dataapi.toString();
        try {
            //parse data
            Object object = jp.parse(dtjson);
            JSONObject jso = (JSONObject) object;
            
            JSONObject data = (JSONObject) jso.get("data");
            JSONObject trx = (JSONObject) data.get("trx");

            SimpleDateFormat dates = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");	
            String trxDate = dates.format(new Date());


            //set parameter to save database
            String idpel = (String) trx.get("subscriber_id");
            String request_date = trxDate;
            String response_date = trxDate;
            String product_id = prodId;
            String rc = (String) trx.get("rc");
            StringBuffer response = dataapi;
            String request = datarequst;

            //cek value rc
            String status = rc.substring(2);
            String strc = null;				
//                        

            if(status.equals("00")){
                strc = "Oke";
            }else if(status.equals("14")){
                strc = "Oke";
            }else if(status.equals("34")){
                strc = "Oke";
            }else if(status.equals("88")){
                strc = "Oke";
            }else{
                strc = "Ganguan";
            }
                      
            //save data
            saveTodatabase(idpel,request_date,response_date,strc,product_id,response,request);
            
            
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public static void saveTodatabase(
        String idpel,
        String request_date,
        String response_date,
        String strc,
        String product_id,
        StringBuffer response,
        String request
    ) throws SQLException{
            String url ="jdbc:postgresql://localhost:5432/txn_project";
            String user="postgres";
            String pass="root";
            con =DriverManager.getConnection(url,user,pass);
            stm = con.createStatement();
            
            //insert into database
            String sql = "INSERT INTO public.product_logs(idpel, request, response, request_date, response_date, status, product_id)VALUES('"+idpel+"', '"+request+"', '"+response+"', '"+request_date+"', '"+response_date+"', '"+strc+"', "+product_id+")";
            if(stm.executeUpdate(sql) == 1){
                stm.close();
                con.close();
                System.out.println("Data berhasil disimpan ...");
            }else{
                System.out.println("Data gagal disimpan ...");
            }

    }
    
    public static void main(String[] args){
        proseshitdataApi();
    }
    
}
