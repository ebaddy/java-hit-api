/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarHitapi;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 *
 * @author e-baddy
 */
public class apiAPixml {

    public static void main(String[] args) {
        try {
            File inputFile = new File("purchase.txt");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("response");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println("-------------------------------------------------------------");
                    System.out.println("mti : " + eElement.getElementsByTagName("mti").item(0).getTextContent());
                    System.out.println("pan : " + eElement.getElementsByTagName("pan").item(0).getTextContent());
                    System.out.println("processing_code : " + eElement.getElementsByTagName("processing_code").item(0).getTextContent());
                    System.out.println("amount : " + eElement.getElementsByTagName("amount").item(0).getTextContent());
                    System.out.println("transmission_date_time : " + eElement.getElementsByTagName("transmission_date_time").item(0).getTextContent());
                    System.out.println("stan : " + eElement.getElementsByTagName("stan").item(0).getTextContent());
                    System.out.println("local_trx_time : " + eElement.getElementsByTagName("local_trx_time").item(0).getTextContent());
                    System.out.println("local_trx_date : " + eElement.getElementsByTagName("local_trx_date").item(0).getTextContent());
                    System.out.println("settlement_date : " + eElement.getElementsByTagName("settlement_date").item(0).getTextContent());
                    System.out.println("merchant_type : " + eElement.getElementsByTagName("merchant_type").item(0).getTextContent());
                    System.out.println("acquiring_institution_id : " + eElement.getElementsByTagName("acquiring_institution_id").item(0).getTextContent());
                    System.out.println("retrieval_ref_no : "+ eElement.getElementsByTagName("retrieval_ref_no").item(0).getTextContent());
                    System.out.println("rc : "+ eElement.getElementsByTagName("rc").item(0).getTextContent());
                    System.out.println("terminal_id : "+ eElement.getElementsByTagName("terminal_id").item(0).getTextContent());
                    System.out.println("acceptor_identification_code : "+ eElement.getElementsByTagName("acceptor_identification_code").item(0).getTextContent());
                    System.out.println("private_data_48 : "+ eElement.getElementsByTagName("private_data_48").item(0).getTextContent());
                    System.out.println("idpel : "+ eElement.getElementsByTagName("idpel").item(0).getTextContent());
                    System.out.println("blth : "+ eElement.getElementsByTagName("blth").item(0).getTextContent());
                    System.out.println("name : "+ eElement.getElementsByTagName("name").item(0).getTextContent());
                    System.out.println("bill_count : "+ eElement.getElementsByTagName("bill_count").item(0).getTextContent());
                    System.out.println("bill_repeat_count : "+ eElement.getElementsByTagName("bill_repeat_count").item(0).getTextContent());
                    System.out.println("rp_tag : "+ eElement.getElementsByTagName("rp_tag").item(0).getTextContent());
                    System.out.println("biaya_admin : "+ eElement.getElementsByTagName("biaya_admin").item(0).getTextContent());
                    
                }
            }
            
            NodeList nListdua = doc.getElementsByTagName("bill");
            System.out.println("------------------ Bills ------------------");
            for (int temp = 0; temp < nListdua.getLength(); temp++) {
                Node nNode = nListdua.item(temp);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println("bill_date : " + eElement.getElementsByTagName("bill_date").item(0).getTextContent());
                    System.out.println("bill_amount : " + eElement.getElementsByTagName("bill_amount").item(0).getTextContent());
                    System.out.println("penalty : " + eElement.getElementsByTagName("penalty").item(0).getTextContent());
                    System.out.println("kubikasi : " + eElement.getElementsByTagName("kubikasi").item(0).getTextContent());
                }
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
