/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarHitapi;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/**
 *
 * @author e-baddy
 */
public class GetAPiparse {
    public static void main(String[] args) {
 
        JSONParser jp = new JSONParser();
        String dtjson="{\"data\":{\"trx\":{\"trx_id\":\"4013298675\",\"trx_type\":\"2100\",\"product_type\":\"BPJS-KESEHATAN\",\"stan\":\"257095\",\"amount\":\"242500\",\"datetime\":\"20190424095138\",\"merchant_code\":\"6012\",\"rc\":\"0000\",\"admin_charge\":\"2500\",\"no_va\":\"0000002426690259  \",\"periode\":\"03\",\"name\":\"OIEJ HAIJ LIEN       (PST:  1)\",\"kode_cabang\":\"1101  \",\"nama_cabang\":\"SEMARANG\",\"premi\":\"240000\",\"sisa\":\"000000000000\"}}}";
 
        try {
            //parse data
            Object object = jp.parse(dtjson);
            JSONObject jso = (JSONObject) object;
 
            JSONObject data = (JSONObject) jso.get("data");
            JSONObject trx = (JSONObject) data.get("trx");
 
            String trx_id = (String) trx.get("trx_id");
            String trx_type = (String) trx.get("trx_type");
            String product_type = (String) trx.get("product_type");
            String stan = (String) trx.get("stan");
            String amount = (String) trx.get("amount");
            
            // Output
            System.out.println("===============================");
            System.out.println("trx_id : "+trx_id);
            System.out.println("trx_type : "+trx_type);
            System.out.println("product_type : "+product_type);
            System.out.println("stan : "+stan);
            System.out.println("amount : "+amount);
            System.out.println("===============================");

        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
