/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarHitapi;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author e-baddy
 */
public class apiGet {
    public static void main(String[] args) {
 
        JSONParser jp = new JSONParser();
 
        try {
            Object object = jp.parse(new FileReader("bpjs.json"));
            JSONObject jso = (JSONObject) object;
 
            JSONObject data = (JSONObject) jso.get("data");
            JSONObject trx = (JSONObject) data.get("trx");
 
            String trx_id = (String) trx.get("trx_id");
            String trx_type = (String) trx.get("trx_type");
            String product_type = (String) trx.get("product_type");
            String stan = (String) trx.get("stan");
            String amount = (String) trx.get("amount");
            
            // Output
            System.out.println("===============================");
            System.out.println("trx_id : "+trx_id);
            System.out.println("trx_type : "+trx_type);
            System.out.println("product_type : "+product_type);
            System.out.println("stan : "+stan);
            System.out.println("amount : "+amount);
            System.out.println("===============================");

        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
