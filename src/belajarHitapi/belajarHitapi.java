/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarHitapi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import java.util.Random;

import java.text.SimpleDateFormat; 
import java.util.Date;  

import java.math.BigInteger; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException; 

import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.applet.Main;

/**
 *
 * @author e-baddy
 */


public class belajarHitapi { 
    
    public static String md5Java(String message) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            //merubah byte array ke dalam String Hexadecimal
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return digest;
    }

    public static void hitApi() {
        System.out.println("===============================================================");
        
    	String username = "plg12110004";
        String password = "aldwin.3";
        String secret_key = "1234";
        String url = "http://180.211.90.243:8118/Transactions/trx.json";
        String trxType = "2100";
        String trxId = "0";
        String custMsisdn = "212100089886";
        String custAccountNo = "212100089886";
        String prodId = "100";
        Random random = new Random();
        trxId = Integer.toString(random.nextInt(99999999));
              
        try {
            URL obj = new URL(url);
            
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setConnectTimeout(100000);
            con.setReadTimeout(100000); 
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");	
            String trxDate = sdf.format(new Date());
    		
            String signature = md5Java(username + password + prodId + trxDate + secret_key);

            System.out.println(trxDate);
            System.out.println(signature);
            System.out.println("\nPELANGIREST username="+username+"&password="+password+"&signature="+signature);

            String urlParameters =
                "trx_date="+trxDate+
                "&trx_type="+trxType+
                "&trx_id="+trxId+
                "&cust_msisdn="+custMsisdn+
                "&cust_account_no="+custAccountNo+
                "&product_id="+prodId+
                "&product_nomination="+
                "&periode_payment=";

            // add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Authorization", "PELANGIREST username="+username+"&password="+password+"&signature="+signature);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");

            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();	

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            // print result
            System.out.println(response.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    
    public static void main(String[] args){
        hitApi();
    }
}
